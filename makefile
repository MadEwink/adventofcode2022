day11: src/day11.cpp bin/MonkeyManager.o bin/Monkey.o
	g++ -o bin/$@ $^ -g

day10: src/day10.cpp bin/CathodeRay.o
	g++ -o bin/$@ $^

day9: src/day9.cpp bin/Rope.o bin/GridPosition.o
	g++ -o bin/$@ $^

day8: src/day8.cpp bin/TreeGrid.o
	g++ -o bin/$@ $^

day7: src/day7.cpp bin/FileSystemNode.o bin/FileNode.o bin/FolderNode.o bin/FileSystemParser.o
	g++ -o bin/$@ $^

day6: src/day6.cpp bin/DeviceSubroutine.o
	g++ -o bin/$@ $^

day5: src/day5.cpp bin/Operation.o bin/Crate.o bin/CrateStack.o bin/Crane.o bin/StorageSite.o
	g++ -o bin/$@ $^

day4: src/day4.cpp bin/SectionAssignment.o bin/CampOrganizer.o
	g++ -o bin/$@ $^

day3: src/day3.cpp bin/ItemHelper.o bin/Rucksack.o bin/RucksackOrganizer.o
	g++ -o bin/$@ $^

day2: src/day2.cpp bin/RockPaperScissors.o bin/StrategyGuide.o
	g++ -o bin/$@ $^

testday2: src/TestDay2.cpp bin/RockPaperScissors.o bin/StrategyGuide.o
	g++ -o bin/$@ $^

day1: src/day1.cpp bin/Snack.o bin/Elf.o bin/CaloriesCounting.o
	g++ -o bin/$@ $^

bin/%.o: src/%.cpp src/%.h
	g++ -o $@ -c $< -g

clean:
	rm bin/*
