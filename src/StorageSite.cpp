#include "StorageSite.h"

#include <iostream>
#include <fstream>

StorageSite::StorageSite(string fileName) {
    readFromFile(fileName);
}

CrateStack& StorageSite::getStack(int id) {
    return stacks[id-1];
}

void StorageSite::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    string line;

    while (getline(inputFile, line)) {
        if (line.find('[') == string::npos) {
            continue;
        }

        int i = 0;
        while (i < line.size()) {
            if (line[i] != '[') {
                i += 4;
                continue;
            }
            
            int stackId = (i / 4) + 1;
            char mark = line[i+1];
            addCrateToStack(stackId, mark);
            i += 4;
        }
    }
}

void StorageSite::addCrateToStack(int stackId, char mark) {
    if (stackId > stacks.size()) {
        stacks.resize(stackId);
    }

    getStack(stackId).initAddCrate(mark);
}

string StorageSite::getTopMarks() {
    string marks("");

    for (int i = 0 ; i < stacks.size() ; i++) {
        marks.append(1, stacks[i].getTopMark());
    }
    
    return marks;
}
