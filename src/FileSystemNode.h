#ifndef DEF_FILESYSTEMNODE
#define DEF_FILESYSTEMNODE

#include <string>

using namespace std;

class FileSystemNode {
    public:
    virtual int getSize();
    string getName();
    protected:
    FileSystemNode(string name);
    string name;
};

#endif
