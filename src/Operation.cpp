#include "Operation.h"

Operation::Operation(int nbMoves, int startStackId, int endStackId) :
    nbMoves(nbMoves), startStackId(startStackId), endStackId(endStackId)
{}

int Operation::getNbMoves() {
    return nbMoves;
}

int Operation::getStartStackId() {
    return startStackId;
}

int Operation::getEndStackId() {
    return endStackId;
}
