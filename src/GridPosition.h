class GridPosition {
    public:
    GridPosition();
    GridPosition(int x, int y);
    GridPosition(const GridPosition& other);
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void moveTowards(const GridPosition& other);
    bool isNeighbour(const GridPosition& other) const;
    bool equals(const GridPosition& other) const;
    bool islower(const GridPosition& other) const;
    bool operator==(const GridPosition& other);
    bool operator<(const GridPosition& other);
    private:
    int x;
    int y;
};
