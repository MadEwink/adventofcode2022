#include "GridPosition.h"
#include "OrderedList.h"

#include <string>

using namespace std;

typedef OrderedList<GridPosition> PositionList;

struct Instruction {
    char dir;
    int nbMoves;
};

class Rope {
    public:
    Rope(string fileName);
    int countVisitedTailPositions();
    int countVisitedTailPositions(int ropeLength);
    private:
    GridPosition headPos;
    GridPosition tailPos;
    vector<Instruction> instructions;
    PositionList visitedTailPositions;

    void readFromFile(string fileName);
    void applyInstruction(Instruction instruction);
    void applyInstruction(Instruction instruction, vector<GridPosition>& ropePos);
    void move(GridPosition& pos, char dir);
    void moveTail(char lastHeadDir);
};
