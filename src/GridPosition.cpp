#include "GridPosition.h"

#include <cstdlib>

GridPosition::GridPosition() :
    x(0),
    y(0)
{}

GridPosition::GridPosition(int x, int y) :
    x(x),
    y(y)
{}

GridPosition::GridPosition(const GridPosition& other) :
    x(other.x),
    y(other.y)
{}

void GridPosition::moveUp() {
    y--;
}

void GridPosition::moveDown() {
    y++;
}

void GridPosition::moveLeft() {
    x--;
}

void GridPosition::moveRight() {
    x++;
}

void GridPosition::moveTowards(const GridPosition& other) {
    if (x < other.x) {
        x++;
    } else if (x > other.x) {
        x--;
    }

    if (y < other.y) {
        y++;
    } else if (y > other.y) {
        y--;
    }
}

bool GridPosition::isNeighbour(const GridPosition& other) const {
    return (abs(x-other.x) <= 1 && abs(y-other.y) <= 1);
}

bool GridPosition::equals(const GridPosition& other) const {
    return other.x == x && other.y == y;
}

bool GridPosition::islower(const GridPosition& other) const {
    if (other.x == x) {
        return y < other.y;
    }

    return x < other.x;
}

bool GridPosition::operator==(const GridPosition& other) {
    return equals(other);
}

bool GridPosition::operator<(const GridPosition& right) {
    return islower(right);
}
