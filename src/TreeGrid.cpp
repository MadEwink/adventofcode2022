#include "TreeGrid.h"

#include <iostream>
#include <fstream>

TreeGrid::TreeGrid(string fileName) {
    readFromFile(fileName);
}

void TreeGrid::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    string curLine;

    while (getline(inputFile, curLine)) {
        if (curLine.empty()) {
            continue;
        }

        vector<int> treeLine;
        for (char c : curLine) {
            treeLine.push_back(int(c-'0'));
        }

        if (gridWidth == 0) {
            gridWidth = treeLine.size();
        } else if (treeLine.size() != gridWidth) {
            throw "Lines length not constant";
        }

        treeGrid.push_back(treeLine);
    }

    gridHeight = treeGrid.size();
}

int TreeGrid::countVisible() {
    int count = 0;
    for (int y = 0 ; y < gridHeight ; y++) {
        for (int x = 0 ; x < gridWidth ; x++) {
            if (isVisible(x, y)) {
                count++;
            }
        }
    }

    return count;
}

bool TreeGrid::isVisible(int x, int y) {
    int height = treeGrid[y][x];

    return isVisibleFromLeft(height, x, y)
        || isVisibleFromRight(height, x, y)
        || isVisibleFromUp(height, x, y)
        || isVisibleFromDown(height, x, y);
}

bool TreeGrid::isVisibleFromLeft(int height, int x, int y) {
    for (int nx = x-1 ; nx >= 0 ; nx--) {
        if (height <= treeGrid[y][nx]) {
            return false;
        }
    }

    return true;
}

bool TreeGrid::isVisibleFromRight(int height, int x, int y) {
    for (int nx = x+1 ; nx < gridWidth ; nx++) {
        if (height <= treeGrid[y][nx]) {
            return false;
        }
    }

    return true;
}

bool TreeGrid::isVisibleFromUp(int height, int x, int y) {
    for (int ny = y-1 ; ny >= 0 ; ny--) {
        if (height <= treeGrid[ny][x]) {
            return false;
        }
    }

    return true;
}

bool TreeGrid::isVisibleFromDown(int height, int x, int y) {
    for (int ny = y+1 ; ny < gridHeight ; ny++) {
        if (height <= treeGrid[ny][x]) {
            return false;
        }
    }

    return true;
}

int TreeGrid::countViewUp(int height, int x, int y) {
    int count = 0;
    for (int ny = y-1 ; ny >= 0 ; ny--) {
        count ++;
        if (height <= treeGrid[ny][x])
            break;
    }
    return count;
}

int TreeGrid::countViewDown(int height, int x, int y) {
    int count = 0;
    for (int ny = y+1 ; ny < gridHeight ; ny++) {
        count ++;
        if (height <= treeGrid[ny][x])
            break;
    }
    return count;
}

int TreeGrid::countViewLeft(int height, int x, int y) {
    int count = 0;
    for (int nx = x-1 ; nx >= 0 ; nx--) {
        count ++;
        if (height <= treeGrid[y][nx])
            break;
    }
    return count;
}

int TreeGrid::countViewRight(int height, int x, int y) {
    int count = 0;
    for (int nx = x+1 ; nx < gridHeight ; nx++) {
        count ++;
        if (height <= treeGrid[y][nx])
            break;
    }
    return count;
}

int TreeGrid::scenicScore(int x, int y) {
    int height = treeGrid[y][x];
    return countViewUp(height, x, y)
        *countViewDown(height, x, y)
        *countViewLeft(height, x, y)
        *countViewRight(height, x, y);
}

int TreeGrid::bestScenicView() {
    int max = 0;
    for (int y = 1 ; y < gridHeight-1 ; y++) {
        for (int x = 1 ; x < gridWidth-1 ; x++) {
            int score = scenicScore(x, y);
            if (score > max) {
                max = score;
            }
        }
    }

    return max;
}
