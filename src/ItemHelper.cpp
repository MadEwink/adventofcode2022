#include "ItemHelper.h"
#include <cctype>

int ItemHelper::getItemPriority(char item) {
    if (std::isupper(item)) {
        return (item - 'A') + 27;
    }

    return (item - 'a') + 1;
}
