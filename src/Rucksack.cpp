#include "Rucksack.h"

using namespace std;

Rucksack::Rucksack(string items) {
    this->items = items;
}

int Rucksack::getItemNumber() {
    return items.size();
}

int Rucksack::getCompartimentSize() {
    return items.size()/COMPARTIMENT_NB;
}

char Rucksack::getCompartmentItem(int compartiment, int i) {
    if (i < 0 || i >= getCompartimentSize()) {
        throw("Invalid index for compartiment access");
    }

    if (compartiment < 0 || compartiment >= COMPARTIMENT_NB) {
        throw("Invalid container index");
    }

    return items[i + compartiment*getCompartimentSize()];
}

char Rucksack::getCommonItem() {
    for (int i = 0 ; i < getCompartimentSize() ; i++) {
        char curItem = getCompartmentItem(0, i);
        if (recursiveSearchItem(1, curItem)) {
            return curItem;
        }
    }

    return ' ';
}

bool Rucksack::recursiveSearchItem(int compartiment, char item) {
    if (compartiment >= COMPARTIMENT_NB) {
        return true;
    }

    for (int i = 0 ; i < getCompartimentSize() ; i++) {
        if (item == getCompartmentItem(compartiment, i)) {
            return recursiveSearchItem(compartiment+1, item);
        }
    }
    
    return false;
}

char Rucksack::getItem(int i) {
    if (i < 0 || i >= getItemNumber()) {
        throw ("Invalid item index");
    }

    return items[i];
}
