#include "Monkey.h"

#include <vector>
#include <string>

class MonkeyManager {
    public:
    MonkeyManager(string fileName);
    long getMonkeyBusinessValue(int nbTurns, int nbMostActive, int relievedValue);
    ~MonkeyManager();
    private:
    int lcmOfTestValues;
    vector<Monkey*> monkeys;
    void readFromFile(string fileName);
    void parseItemLine(string curLine, int itemsPos);
    void parseOperationLine(string curLine, int operationPos, MonkeyOperationType& curOperationType, int& curOperationValue);
    int parseTestLine(string curLine, int testPos);
    void parseThrowLine(string curLine, int throwPos, int& curTargetTrue, int& curTargetFalse);
    void round(int relievedValue);
    void monkeyTurn(Monkey* monkey, int relievedValue);
};
