#include <iostream>
#include <string>
#include "DeviceSubroutine.h"

int main(int argc, char** argv) {
    string fileName("input/input-6");
    if (argc > 1) {
        fileName = argv[1];
    }

    DeviceSubroutine deviceSubroutine(fileName);

    cout << "Start packet index : " << deviceSubroutine.getStartOfPacket(4) << endl;

    cout << "Start message index : " << deviceSubroutine.getStartOfPacket(14) << endl;

    return 0;
}

