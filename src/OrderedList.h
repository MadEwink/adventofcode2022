#include <vector>

using namespace std;

template <typename T>
class OrderedList {
    public:
    OrderedList() {}
    void insertSort(T newElement) {
        for (vector_iterator i = list.begin() ; i != list.end() ; i++) {
            if (newElement == *i) {
                return;
            }
            if (newElement < *i) {
                list.insert(i, newElement);
                return;
            }
        }

        list.push_back(newElement);
    }
    typedef typename vector<T>::iterator vector_iterator;
    vector_iterator begin() { return list.begin(); }
    vector_iterator end() { return list.end(); }
    int getSize() { return list.size(); }
    void clear() { list.clear(); }
    private:
    vector<T> list;
};
