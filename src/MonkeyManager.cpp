#include "MonkeyManager.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>

MonkeyManager::MonkeyManager(string fileName) {
    readFromFile(fileName);
}

long MonkeyManager::getMonkeyBusinessValue(int nbTurns, int nbMostActive, int relievedValue) {
    for (int i = 0; i < nbTurns ; i++) {
        round(relievedValue);
    }

    // get all totalItemsInspected values sorted
    vector<int> totalItemsInspected;
    for (Monkey* monkey : monkeys) {
        totalItemsInspected.push_back(monkey->getTotalItemsInspected());
    }
    sort(totalItemsInspected.rbegin(), totalItemsInspected.rend());


    long businessValue = 1;
    for (int i = 0 ; i < nbMostActive && i < totalItemsInspected.size() ; i++) {
        businessValue *= totalItemsInspected[i];
    }

    return businessValue;
}

void MonkeyManager::round(int relievedValue) {
    int i = 0;
    for (Monkey* monkey : monkeys) {
        monkeyTurn(monkey, relievedValue);
    }
}

void MonkeyManager::monkeyTurn(Monkey* monkey, int relievedValue) {
    while (monkey->hasItemsLeft()) {
        monkey->inspectItem(relievedValue, lcmOfTestValues);
        int receiver = 0;
        if (monkey->testItem()) {
            receiver = monkey->getTargetTrue();
        } else {
            receiver = monkey->getTargetFalse();
        }

        long item = monkey->popItem();
        monkeys[receiver]->grabItem(item);
    }
}

void MonkeyManager::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    monkeys.clear();

    string curLine;

    MonkeyOperationType curOperationType;
    int curOperationValue;
    int curTestValue;
    int curTargetTrue;
    int curTargetFalse;
    lcmOfTestValues = 1;

    while (getline(inputFile, curLine)) {
        if (curLine.empty()) {
            monkeys.back()->init(curOperationType, curOperationValue, curTestValue, curTargetTrue, curTargetFalse);
            lcmOfTestValues = lcm(curTestValue, lcmOfTestValues);
            continue;
        }

        if (curLine[0] == 'M') {
            monkeys.push_back(new Monkey());
            continue;
        }
            
        int itemsPos = curLine.find("items");
        if (itemsPos != string::npos) {
            parseItemLine(curLine, itemsPos);
            continue;
        }

        int operationPos = curLine.find("old");
        if (operationPos != string::npos) {
            parseOperationLine(curLine, operationPos, curOperationType, curOperationValue);
            continue;
        }

        int testPos = curLine.find("divisible");
        if (testPos != string::npos) {
            curTestValue = parseTestLine(curLine, testPos);
            continue;
        }

        int throwPos = curLine.find("monkey");
        if (throwPos != string::npos) {
            parseThrowLine(curLine, throwPos, curTargetTrue, curTargetFalse);
            continue;
        }
    }
}

void MonkeyManager::parseItemLine(string curLine, int itemsPos) {
    int curPos = itemsPos + 7;
    while (curPos != string::npos) {
        int nextPos = curLine.find(',', curPos+1);
        if (nextPos > 0) {
            int item = stoi(curLine.substr(curPos, nextPos-curPos));
            monkeys.back()->grabItem(item);
            curPos = nextPos+1;
        } else {
            int item = stoi(curLine.substr(curPos));
            monkeys.back()->grabItem(item);
            curPos = nextPos;
        }
    }
}

void MonkeyManager::parseOperationLine(string curLine, int operationPos, MonkeyOperationType& curOperationType, int& curOperationValue) {
    operationPos += 4;
    bool isDoubleOrSquare = curLine.find("old", operationPos) != string::npos;
    switch(curLine[operationPos]) {
        case '+':
            if (isDoubleOrSquare) {
                curOperationType = Double;
                return;
            }
            curOperationType = Addition;
            break;
        case '*':
            if (isDoubleOrSquare) {
                curOperationType = Square;
                return;
            }
            curOperationType = Multiplication;
            break;
        default:
            throw "Unkown operation";
    }

    operationPos += 2;
    curOperationValue = stoi(curLine.substr(operationPos));
}

int MonkeyManager::parseTestLine(string curLine, int testPos) {
    testPos += 13;
    return stoi(curLine.substr(testPos));
}

void MonkeyManager::parseThrowLine(string curLine, int throwPos, int& curTargetTrue, int& curTargetFalse) {
    bool isTrue = curLine.find("true") != string::npos;
    throwPos += 7;
    int target = stoi(curLine.substr(throwPos));
    if (isTrue) {
        curTargetTrue = target;
    } else {
        curTargetFalse = target;
    }
}

MonkeyManager::~MonkeyManager() {
    for (Monkey* monkey : monkeys) {
        delete monkey;
    }
}
