#include <vector>
#include <string>
#include "CrateStack.h"

using namespace std;

class StorageSite {
    public:
    StorageSite(string fileName);
    CrateStack& getStack(int id);
    string getTopMarks();
    private:
    vector<CrateStack> stacks;
    void readFromFile(string fileName);
    void addCrateToStack(int stackId, char mark);
};
