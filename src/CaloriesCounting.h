#include <vector>
#include <string>
#include "Elf.h"

class CaloriesCounting {
    public:
    CaloriesCounting();
    int getMaxCaloriesFromInput(string fileName);
    private:
    vector<Elf> elves;
    void readInput(string fileName);
    int getMaxCalories();
    int getNMaxCaloriesSum(int n);
};
