#include <iostream>
#include <string>
#include "CaloriesCounting.h"

int main(int argc, char** argv) {
    string fileName("input/input-1");
    if (argc > 1) {
        fileName = argv[1];
    }

    CaloriesCounting calCount;
    int maxCal = calCount.getMaxCaloriesFromInput(fileName);
    if (maxCal < 0) {
        return -1;
    }

    cout << "Nombre max de calories : " << maxCal << endl;

    return 0;
}
