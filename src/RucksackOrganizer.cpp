#include "RucksackOrganizer.h"
#include "ItemHelper.h"

RucksackOrganizer::RucksackOrganizer(string fileName) {
    readFromFile(fileName);
}

void RucksackOrganizer::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    string line;

    while (getline(inputFile, line)) {
        rucksacks.push_back(Rucksack(line));
    }
}

int RucksackOrganizer::getCommonPrioritySum() {
    int totalPriority = 0;

    for (int i = 0 ; i < rucksacks.size() ; i ++) {
        char commonItem = rucksacks[i].getCommonItem();
        totalPriority += ItemHelper::getItemPriority(commonItem);
    }

    return totalPriority;
}

int RucksackOrganizer::getBadgePrioritySum() {
    int totalPriority = 0;

    for (int i = 0 ; i < getGroupNumber() ; i++) {
        char badge = findBadgeFromGroup(i);
        totalPriority += ItemHelper::getItemPriority(badge);
    }

    return totalPriority;
}

int RucksackOrganizer::getGroupNumber() {
    return rucksacks.size()/GROUP_SIZE;
}

Rucksack RucksackOrganizer::getGroupRucksack(int group, int member) {
    return rucksacks[group*GROUP_SIZE + member];
}

char RucksackOrganizer::findBadgeFromGroup(int group) {
    Rucksack rucksack = getGroupRucksack(group, 0);
    for (int i = 0 ; i < rucksack.getItemNumber() ; i++) {
        char badge = rucksack.getItem(i);
        if (recursiveSearchBadge(group, 1, badge)) {
            return badge;
        }
    }

    return ' ';
}

bool RucksackOrganizer::recursiveSearchBadge(int group, int member, char badge) {
    if (member >= GROUP_SIZE) {
        return true;
    }

    Rucksack rucksack = getGroupRucksack(group, member);

    for (int i = 0 ; i < rucksack.getItemNumber() ; i++) {
        if (badge == rucksack.getItem(i)) {
            return recursiveSearchBadge(group, member+1, badge);
        }
    }

    return false;
}
