#include "FolderNode.h"
#include "FileNode.h"

#include <typeinfo>

FolderNode::FolderNode(string name, FolderNode* parent) :
    parent(parent),
    FileSystemNode(name) {}

FolderNode::FolderNode(string name) :
    parent(NULL),
    FileSystemNode(name) {}

int FolderNode::getSize() {
    int size = 0;
    for (FileSystemNode* child : children) {
        size += child->getSize();
    }
    return size;
}

FolderNode* FolderNode::getOrCreateChildFolder(string childName) {
    for (FileSystemNode* child : children) {
        if (typeid(*child) != typeid(FolderNode)) {
            continue;
        }
        if (child->getName() == childName) {
            return (FolderNode*)child;
        }
    }

    FolderNode* newChild = new FolderNode(childName, this);
    children.push_back(newChild);
    return newChild;
}

void FolderNode::addChildFolder(string newChildName) {
    for (FileSystemNode* child : children) {
        if (child->getName() == newChildName) {
            return;
        }
    }

    children.push_back(new FolderNode(newChildName, this));
}

void FolderNode::addChildFile(string newChildName, int size) {
    for (FileSystemNode* child : children) {
        if (child->getName() == newChildName) {
            return;
        }
    }

    children.push_back(new FileNode(newChildName, size));
}

FolderNode* FolderNode::getParent() {
    return parent;
}

FolderNode::~FolderNode() {
    for (FileSystemNode* child : children) {
        delete child;
    }
}

int FolderNode::getSizeIfLessThan(int maxSize) {
    int size = getSize();
    if (size > maxSize) {
        size = 0;
    }

    for (FileSystemNode* child : children) {
        if (typeid(*child) != typeid(FolderNode)) {
            continue;
        }
        size += ((FolderNode*)child)->getSizeIfLessThan(maxSize);
    }
    
    return size;
}

int FolderNode::getMinBiggerThan(int min) {
    int size = getSize();

    for (FileSystemNode* child : children) {
        if (typeid(*child) != typeid(FolderNode)) {
            continue;
        }
        int childSize = ((FolderNode*)child)->getMinBiggerThan(min);
        if (childSize >= min && childSize < size) {
            size = childSize;
        }
    }

    return size;
}
