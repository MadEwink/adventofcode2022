#include "SectionAssignment.h"

SectionAssignment::SectionAssignment(int minId, int maxId) :
    minSectionId(minId), maxSectionId(maxId)
{}

bool SectionAssignment::contains(SectionAssignment other) {
    return minSectionId <= other.minSectionId && maxSectionId >= other.maxSectionId;
}

bool SectionAssignment::overlaps(SectionAssignment other) {
    return (minSectionId >= other.minSectionId && minSectionId <= other.maxSectionId) || (maxSectionId >= other.minSectionId && maxSectionId <= other.maxSectionId) || contains(other);
}
