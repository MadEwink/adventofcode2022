#include "FileSystemParser.h"
#include "FileNode.h"

#include <iostream>
#include <fstream>

FileSystemParser::FileSystemParser() :
    root("/")
{
    currentFolder = &root;
}

void FileSystemParser::parseFileSystem(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    string curLine;

    while (getline(inputFile, curLine)) {
        if (curLine.empty()) {
            continue;
        }

        if (curLine[0] == '$') {
            parseCommand(curLine);
        } else {
            parseFile(curLine);
        }
    }
}

void FileSystemParser::parseCommand(string commandLine) {
    string command;
    int spacePos = commandLine.find(' ', 2);
    if (spacePos < 0) {
        command = commandLine.substr(2);
    } else {
        command = commandLine.substr(2, spacePos-2);
    }

    if (command == "cd") {
        string argument = commandLine.substr(5);
        if (argument == "..") {
            currentFolder = currentFolder->getParent();
        } else if (argument == "/") {
            currentFolder = &root;
        } else {
            currentFolder = currentFolder->getOrCreateChildFolder(argument);
        }
    } else if (command != "ls") {
        throw "Unknown command";
    }
}

void FileSystemParser::parseFile(string fileLine) {
    int spacePos = fileLine.find(' ');
    string firstTerm = fileLine.substr(0, spacePos);
    string secondTerm = fileLine.substr(spacePos);

    if (firstTerm == "dir") {
        currentFolder->addChildFolder(secondTerm);
    } else {
        int size = stoi(firstTerm);
        currentFolder->addChildFile(secondTerm, size);
    }
}

int FileSystemParser::getTotalSize() {
    return root.getSize();
}

int FileSystemParser::getSumOfSizeLessThan(int maxSizeToConsider) {
    return root.getSizeIfLessThan(maxSizeToConsider);
}

int FileSystemParser::getSmallestDirectoryForSpace(int totalSpace, int desiredAvailable) {
    int used = getTotalSize();

    int minToFree = desiredAvailable - (totalSpace - used);

    return root.getMinBiggerThan(minToFree);
}
