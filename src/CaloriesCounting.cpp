#include "CaloriesCounting.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

CaloriesCounting::CaloriesCounting() {}

int CaloriesCounting::getMaxCaloriesFromInput(string fileName) {
    elves.push_back(Elf());

    try {
        readInput(fileName);
    } catch(...) {
        return -1;
    }

    return getNMaxCaloriesSum(3);

}

int CaloriesCounting::getMaxCalories() {
    int maxCalories = 0;

    for (int i = 0 ; i < elves.size() ; i ++) {
        int curCalories = elves[i].getTotalCalories();
        if (curCalories > maxCalories) {
            maxCalories = curCalories;
        }
    }

    return maxCalories;
}

int CaloriesCounting::getNMaxCaloriesSum(int n) {
    vector<int> maxCalories(n, 0);

    for (int i = 0 ; i < elves.size() ; i ++) {
        int curCalories = elves[i].getTotalCalories();
        for (int j = 0 ; j < n ; j++) {
            if (curCalories > maxCalories[j]) {
                for (int k = n-2 ; k >= j ; k--) {
                    maxCalories[k+1] = maxCalories[k];
                }
                maxCalories[j] = curCalories;
                break;
            }
        }
    }

    int total = 0;
    for (int i = 0 ; i < n ; i++) {
        total += maxCalories[i];
    }
    
    return total;
}

void CaloriesCounting::readInput(string fileName) {
    string curLine;
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    while (getline(inputFile, curLine)) {
        if (curLine.empty()) {
            elves.push_back(Elf());
        } else {
            int calories = stoi(curLine);
            elves[elves.size()-1].addSnack(Snack(calories));
        }
    }
}
