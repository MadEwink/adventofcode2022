#include <string>

using namespace std;

class DeviceSubroutine {
    public:
    DeviceSubroutine(string fileName);
    int getStartOfPacket(int markerLength);
    private:
    string stream;
    void readStream(string fileName);
};
