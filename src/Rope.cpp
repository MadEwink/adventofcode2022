#include "Rope.h"

#include <iostream>
#include <fstream>

Rope::Rope(string fileName) {
    readFromFile(fileName);
}

int Rope::countVisitedTailPositions() {
    visitedTailPositions.clear();
    visitedTailPositions.insertSort(tailPos);

    for (Instruction& instruction : instructions) {
        applyInstruction(instruction);
    }

    return visitedTailPositions.getSize();
}

int Rope::countVisitedTailPositions(int ropeLength) {
    vector<GridPosition> ropePos;
    ropePos.resize(ropeLength);

    visitedTailPositions.clear();
    visitedTailPositions.insertSort(ropePos.back());

    for (Instruction& instruction : instructions) {
        applyInstruction(instruction, ropePos);
    }

    return visitedTailPositions.getSize();
}

void Rope::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    string curLine;

    while (getline(inputFile, curLine)) {
        if (curLine.empty()) {
            continue;
        }

        Instruction instruction = {
            curLine[0],
            stoi(curLine.substr(2))
        };

        instructions.push_back(instruction);
    }
}


void Rope::applyInstruction(Instruction instruction) {
    for (int i = 0 ; i < instruction.nbMoves ; i++) {
        move(headPos, instruction.dir);
        moveTail(instruction.dir);
    }
}

void Rope::applyInstruction(Instruction instruction, vector<GridPosition>& ropePos) {
    for (int i = 0 ; i < instruction.nbMoves ; i++) {
        move(ropePos[0], instruction.dir);
        for (int i = 1 ; i < ropePos.size() ; i++) {
            if (ropePos[i].isNeighbour(ropePos[i-1])) {
                break;
            }
            ropePos[i].moveTowards(ropePos[i-1]);
        }
        visitedTailPositions.insertSort(ropePos.back());
    }
}

void Rope::move(GridPosition& pos, char dir) {
    switch (dir) {
        case 'U':
            pos.moveUp();
            break;
        case 'D':
            pos.moveDown();
            break;
        case 'L':
            pos.moveLeft();
            break;
        case 'R':
            pos.moveRight();
            break;
        default:
            throw "Invalid instruction";
    }
}

void Rope::moveTail(char lastHeadDir) {
    if (tailPos.isNeighbour(headPos)) {
        return;
    }

    tailPos.moveTowards(headPos);
    visitedTailPositions.insertSort(tailPos);
}
