#include "DeviceSubroutine.h"

#include <iostream>
#include <fstream>

DeviceSubroutine::DeviceSubroutine(string fileName) {
    readStream(fileName);
}

void DeviceSubroutine::readStream(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    getline(inputFile, stream);
}

int DeviceSubroutine::getStartOfPacket(int markerLength) {
    // dumb search
    int i;
    for (i = markerLength ; i < stream.size() ; i++) {
        bool duplicate = false;
        for (int j = -markerLength ; j < 0 ; j++) {
            char curChar = stream[i+j];
            for (int k = 1 ; k < -j ; k++) {
                if (curChar == stream[i+j+k]) {
                    duplicate = true;
                    break;
                }
            }
            if (duplicate) {
                break;
            }
        }

        if (!duplicate) {
            return i;
        }
    }

    return -1;
}
