#include "StrategyGuide.h"

int main(int argc, char** argv) {
    if (StrategyGuide::getMoveValue(Rock) != 1) return -1;
    if (StrategyGuide::getMoveValue(Paper) != 2) return -1;
    if (StrategyGuide::getMoveValue(Scissors) != 3) return -1;

    if (RPSGame::resolve(Rock, Rock) != 0) return -1;
    if (RPSGame::resolve(Paper, Paper) != 0) return -1;
    if (RPSGame::resolve(Scissors, Scissors) != 0) return -1;

    if (RPSGame::resolve(Rock, Paper) != 1) return -1;
    if (RPSGame::resolve(Rock, Scissors) != -1) return -1;
    if (RPSGame::resolve(Paper, Rock) != -1) return -1;
    if (RPSGame::resolve(Paper, Scissors) != 1) return -1;
    if (RPSGame::resolve(Scissors, Rock) != 1) return -1;
    if (RPSGame::resolve(Scissors, Paper) != -1) return -1;

    if (StrategyGuide::computeResponseScore(Rock, Rock) != 4) return -1;
    if (StrategyGuide::computeResponseScore(Paper, Paper) != 5) return -1;
    if (StrategyGuide::computeResponseScore(Scissors, Scissors) != 6) return -1;

    if (StrategyGuide::computeResponseScore(Rock, Paper) != 8) return -1;
    if (StrategyGuide::computeResponseScore(Rock, Scissors) != 3) return -1;
    if (StrategyGuide::computeResponseScore(Paper, Rock) != 1) return -1;
    if (StrategyGuide::computeResponseScore(Paper, Scissors) != 9) return -1;
    if (StrategyGuide::computeResponseScore(Scissors, Rock) != 7) return -1;
    if (StrategyGuide::computeResponseScore(Scissors, Paper) != 2) return -1;

    return 0;
}
