#include "CampOrganizer.h"

#include <iostream>
#include <fstream>

CampOrganizer::CampOrganizer(string fileName) {
    readFromFile(fileName);
}

int CampOrganizer::countFullyContainingAssignments() {
    int totalContainingPairs = 0;

    // iterate on pairs
    for (int i = 0 ; i < assignments.size()>>1 ; i++) {
        int j = 2*i;
        if (assignments[j].contains(assignments[j+1]) || assignments[j+1].contains(assignments[j])) {
            totalContainingPairs++;
        }
    }

    return totalContainingPairs;
}

int CampOrganizer::countOverlappingAssignments() {
    int totalOverlappingPairs = 0;

    // iterate on pairs
    for (int i = 0 ; i < assignments.size()>>1 ; i++) {
        int j = 2*i;
        if (assignments[j].overlaps(assignments[j+1])) {
            totalOverlappingPairs++;
        }
    }

    return totalOverlappingPairs;
}

void CampOrganizer::readFromFile(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    string line;

    while (getline(inputFile, line)) {
        int firstSeparatorPos = line.find('-');
        int pairSeparatorPos = line.find(',');
        int secondSeparatorPos = line.find('-', pairSeparatorPos);

        assignments.push_back(SectionAssignment(stoi(line.substr(0, firstSeparatorPos)), stoi(line.substr(firstSeparatorPos+1, pairSeparatorPos-firstSeparatorPos-1))));

        assignments.push_back(SectionAssignment(stoi(line.substr(pairSeparatorPos+1, secondSeparatorPos-pairSeparatorPos-1)), stoi(line.substr(secondSeparatorPos+1))));
    }
};
