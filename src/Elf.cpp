#include "Elf.h"
#include <vector>

using namespace std;

Elf::Elf() {
}

void Elf::addSnack(Snack snack) {
    snacks.push_back(snack);
}

int Elf::getTotalCalories() {
    int total = 0;
    for (int i = 0 ; i < snacks.size() ; i ++) {
        total += snacks[i].getCalories();
    }
    return total;
}
