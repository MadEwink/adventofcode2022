#include <iostream>
#include <string>
#include "RucksackOrganizer.h"

int main(int argc, char** argv) {
    string fileName("input/input-3");
    if (argc > 1) {
        fileName = argv[1];
    }

    RucksackOrganizer rucksackOrganizer(fileName);

    int priority = rucksackOrganizer.getCommonPrioritySum();

    int badgePriority = rucksackOrganizer.getBadgePrioritySum();

    cout << "Priority : " << priority << endl;
    cout << "Badge priority : " << badgePriority << endl;

    return 0;
}
