#include "FileNode.h"

FileNode::FileNode(string name, int size) :
    size(size),
    FileSystemNode(name)
{}

int FileNode::getSize() {
    return size;
}
