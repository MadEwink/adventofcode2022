#include <string>
#include <vector>
#include "RockPaperScissors.h"

using namespace std;

class StrategyGuide {
    public:
    StrategyGuide(string fileName);
    int computeScore();
    static RockPaperScissors translateOpponentFromGuide(char guideMove);
    static RockPaperScissors translateResponseFromGuide(char guideMove);
    static RockPaperScissors getResponseFromGuideInstruction(RockPaperScissors opponentMove, char guideInstruction);
    static int getMoveValue(RockPaperScissors move);
    static int computeResponseScore(RockPaperScissors opponent, RockPaperScissors response);
    private:
    vector<RockPaperScissors> opponentMoves;
    vector<RockPaperScissors> responseMoves;
    void readFromFile(string fileName);
};
