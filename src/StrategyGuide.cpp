#include "StrategyGuide.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

StrategyGuide::StrategyGuide(string fileName) {
    readFromFile(fileName);
}

void StrategyGuide::readFromFile(string fileName) {
    ifstream inputFile(fileName);
    
    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    while (!inputFile.eof()) {
        char guideMove = inputFile.get();
        switch(guideMove) {
            case 'A':
            case 'B':
            case 'C':
                opponentMoves.push_back(translateOpponentFromGuide(guideMove));
                break;
            case 'X':
            case 'Y':
            case 'Z':
                responseMoves.push_back(getResponseFromGuideInstruction(opponentMoves[opponentMoves.size()-1], guideMove));
                break;
            default:
                break;
        }
    }

    if (opponentMoves.size() != responseMoves.size()) {
        throw("Error : move numbers don't match");
    }
}

int StrategyGuide::computeScore() {
    int totalScore = 0;
    for (int i = 0 ; i < this->opponentMoves.size() ; i++) {
        totalScore += computeResponseScore(opponentMoves[i], responseMoves[i]);
    }
    return totalScore;
}

int StrategyGuide::computeResponseScore(RockPaperScissors opponent, RockPaperScissors response) {
    int v =  getMoveValue(response) + 6 + 3*(RPSGame::resolve(opponent, response)-1);
    return v;
}

int StrategyGuide::getMoveValue(RockPaperScissors move) {
    switch(move) {
        case Rock:
            return 1;
        case Paper:
            return 2;
        case Scissors:
            return 3;
        default:
            return 0;
    }
}

RockPaperScissors StrategyGuide::translateOpponentFromGuide(char guideMove) {
    switch (guideMove) {
        case 'A':
            return Rock;
        case 'B':
            return Paper;
        case 'C':
            return Scissors;
        default:
            throw("Invalid move from guide");
    }
}

RockPaperScissors StrategyGuide::translateResponseFromGuide(char guideMove) {
    switch (guideMove) {
        case 'X':
            return Rock;
        case 'Y':
            return Paper;
        case 'Z':
            return Scissors;
        default:
            throw("Invalid move from guide");
    }
}

RockPaperScissors StrategyGuide::getResponseFromGuideInstruction(RockPaperScissors opponentMove, char guideInstruction) {
    switch (guideInstruction) {
        case 'X':
            return static_cast<RockPaperScissors>((opponentMove+2)%3);
        case 'Y':
            return static_cast<RockPaperScissors>(opponentMove);
        case 'Z':
            return static_cast<RockPaperScissors>((opponentMove+1)%3);
        default:
            throw("Invalid instruction from guide");
    }
}
