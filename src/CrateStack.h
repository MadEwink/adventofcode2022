#include <deque>
#include "Crate.h"

using namespace std;

class CrateStack {
    public:
    CrateStack();
    void initAddCrate(char mark);
    Crate popCrate();
    void addCrate(Crate crate);
    char getTopMark();
    private:
    deque<Crate> crates;
};
