#include "RockPaperScissors.h"

int RPSGame::resolve(RockPaperScissors a, RockPaperScissors b) {
    if (a == b) return 0;
    if ( (a+3-b)%3 == 1) return -1;
    return 1;
}
