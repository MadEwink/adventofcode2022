class Snack {
    public:
    Snack(int calories);
    int getCalories();
    private:
    int calories;
};
