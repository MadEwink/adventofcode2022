#include <string>

using namespace std;

class CathodeRay {
    public:
    CathodeRay() {}
    int getSignalStrengthSum(string fileName, int initStep, int stepSize);
    private:
    int nextValueChange;
    int timeBuffer;
};
