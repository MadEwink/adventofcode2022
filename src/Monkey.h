#include <deque>

using namespace std;

enum MonkeyOperationType {
    Addition,
    Multiplication,
    Double,
    Square
};

class Monkey {
    public:
    Monkey();
    // insert items at front
    void grabItem(long item);
    void init(MonkeyOperationType operationType, int operationValue, int testValue, int targetTrue, int targetFalse);
    bool hasItemsLeft();
    // inspect and pop from the back
    void inspectItem(int relievedValue, int globalMod);
    bool testItem();
    long popItem();
    int getTotalItemsInspected();
    int getTargetTrue();
    int getTargetFalse();
    private:
    int totalItemsInspected;
    deque<long> items;
    MonkeyOperationType operationType;
    int operationValue;
    int testValue;
    int targetTrue;
    int targetFalse;
};
